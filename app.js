const ddg = require('ddg');
const TelegramBot = require('node-telegram-bot-api');

//Terminal in telegram

/* Telegram side
DESCRIPTION
Bot that sends information about searched thing based in DuckDuckGo API.
ABOUT
I can explain some things for you.
BOTPIC
botpic_duck.jpg
CREATE Commands to auto complete
start - show help
ddg - plus duckduckgo for example
----------------------------------------------------------
*/

//BOT CONFIGURING
const token = 'YOUT_TELEGRAM_BOT_TOKEN';
const bot = new TelegramBot(token, {polling: true});


bot.onText(/\/ddg (.+)/, (msg, match) => {
  const q = match[1]; //Captured question

  ddg.query(q, function(err, data) {
    if (err) throw err; //Errors

    const answer = data.AbstractText; //Get AbstractText from data and save it to the answer variable

    console.log(data.AbstractText + "\n"); //Print AbstractText to terminal
    bot.sendMessage(msg.chat.id, answer);
  });
});

bot.onText(/\/start/, (msg) => {
  const hostname = os.hostname();
  bot.sendMessage(msg.chat.id, "Send message /ddg DuckDuckGo to get information about duckduckgo. Keep in mind that you can change DuckDuckGo to anything else. For example nodejs.");
});
